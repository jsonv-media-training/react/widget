import React, { Fragment, FunctionComponent } from 'react';
import { v4 as uuid } from 'uuid';

import './App.css';

import AccordionWrapper from "../components/accordion/AccordionWrapper";
import AccordionWrapperV2 from "../components/accordion-v2/AccordionWrapper";
import Search from "../components/search/Search";
import SearchV2 from "../components/search-v2/Search";
import Dropdown from "../components/dropdown/Dropdown";
import Translate from "../components/translate/Translate";

import { DropdownOption } from "../components/dropdown/model";
import { AccordionItem } from "../components/accordion/model";
import { AccordionToggleMode } from "../components/accordion-v2/model";
import Router from "../components/router/Router";
import SideBar from "../components/sidebar/SideBar";
import { Route } from "../components/sidebar/model";

const items: AccordionItem[] = [
  {
    id: uuid().toString(),
    title: 'Hey there, what up! Hey there, what up! Hey there, what up!',
    text: 'Nothing',
  },
  {
    id: uuid().toString(),
    title: 'I am learning react',
    text: 'It is so annoying',
  },
  {
    id: uuid().toString(),
    title: 'If it takes eight men ten hours to build a wall, how long would it take four men?',
    text: 'No time, because the wall is already built.',
    isOpen: true
  },
  {
    id: uuid().toString(),
    title: 'What two things can you never eat for breakfast?',
    text: 'Lunch and dinner. Lunch and dinner. Lunch and dinner. Lunch and dinner. Lunch and dinner. Lunch and dinner. Lunch and dinner. Lunch and dinner.',
    isOpen: true
  }
];

const options: DropdownOption<string>[] = [
  {
    label: 'Green',
    value: 'green',
  },
  {
    label: 'Red',
    value: 'red',
  },
  {
    label: 'Yellow',
    value: 'yellow'
  },
  {
    label: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard a aa a dummy text ever since the 1500s',
    value: 'Lorem Ipsum'
  }
];

const languageOptions: DropdownOption<string>[] = [
  {
    label: 'Korean',
    value: 'ko',
  },
  {
    label: 'Arabic',
    value: 'ar',
  },
  {
    label: 'Hindi',
    value: 'hi',
  },
  {
    label: 'French',
    value: 'fr',
  },
  {
    label: 'Vietnamese',
    value: 'vi',
  },
]

const routes: Route[] = [
  {
    label: 'Accordion Component',
    path: '/'
  },
  {
    label: 'Search Component',
    path: '/search'
  },
  {
    label: 'Dropdown Component',
    path: '/dropdown'
  },
  {
    label: 'Translate Component',
    path: '/translate'
  }
]

const App: FunctionComponent<any> = () => {
  return (
    <Fragment>
      <SideBar items={routes}/>
      <br/><br/>
      <Router path="/">
        <h4 className="text-center mb-4">Accordion Component</h4>
        <h5>Local State</h5>
        <AccordionWrapper items={items}/>
        <br/>
        <h5>Parent State</h5>
        <AccordionWrapperV2 items={items} toggleMode={AccordionToggleMode.SINGLE_MODE}/>
        <br/>
        <h5>Parent State with AutoClose</h5>
        <AccordionWrapperV2 items={items} toggleMode={AccordionToggleMode.AUTOCLOSE_MODE}/>
      </Router>
      <Router path={'/search'}>
        <h4 className="text-center mb-4">Search Component</h4>
        <Search/>
        <SearchV2 />
      </Router>
      <Router path={'/dropdown'}>
        <h4 className="text-center mb-4">Dropdown Component</h4>
        <Dropdown
          label={'Color'}
          options={options}
          selectedIndex={2}
          onOptionSelected={(e: any, selected: any) => console.log(selected)} />
      </Router>
      <Router path={'/translate'}>
        <h4 className="text-center mb-4">Translate Component</h4>
        <Translate options={languageOptions}/>
      </Router>
    </Fragment>
  );
};

export default App;
