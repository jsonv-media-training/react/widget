import { RefObject, useEffect, useState } from "react";

export const useOnClickOutSide = (ref: RefObject<HTMLElement>, handler: Function) => {
  useEffect(() => {
    const onClickOutSide = (event: any) => {
      if (!ref.current || (ref.current && ref.current.contains(event.target))) {
        // no ref or the current click event is happened inside the target element
        return;
      }
      console.log('click outside');
      handler(event);
    }
    document.addEventListener('click', onClickOutSide);
    return () => document.removeEventListener('click', onClickOutSide);
  }, [ref, handler])
}

export const useElementFocus = (ref: RefObject<HTMLElement>) => {
  const [isFocus, isFocusSetter] = useState<boolean>(ref.current === document.activeElement);

  useEffect(() => {
    const isFocusHandler = (e: any) => {
      if (ref.current === document.activeElement) {
        isFocusSetter(true);
        return;
      }
      isFocusSetter(false);
    }
    document.addEventListener('click', isFocusHandler);
    return () => document.removeEventListener('click', isFocusHandler);
  }, [ref]);

  return isFocus;
}