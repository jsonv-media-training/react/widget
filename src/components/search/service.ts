import axios from "axios";
import { Res, WikiResult } from "./model";

export const searchWiki = async (term: string): Promise<Res<WikiResult>> => {
  console.log('search', term)
  return axios.get(
    `https://en.wikipedia.org/w/api.php`,
    {
      params: {
        action: 'query',
        list: 'search',
        format: 'json',
        origin: '*',
        srsearch: term
      }
    }
  );
};


