import React, {FunctionComponent, useEffect, useState, Fragment, useRef, createRef} from "react";

import './Search.scss';
import { searchWiki } from "./service";
import { WikiResult } from "./model";
import CSSTransition from "react-transition-group/CSSTransition";
import TransitionGroup from "react-transition-group/TransitionGroup";

const Search: FunctionComponent<any> = (props) => {
  const [searchTerm, searchTermSetter] = useState<string>('');
  const [loading, loadingSetter] = useState<boolean>(false);
  const [result, resultSetter] = useState<WikiResult>({} as WikiResult);

  const lastSearchKeyWordRef = useRef<string>('');

  useEffect(() => {
    if (!searchTerm || searchTerm === lastSearchKeyWordRef.current) return;
    loadingSetter(true);
    const timeOutId = setTimeout( async () => {
      const { data } = await searchWiki(searchTerm);
      lastSearchKeyWordRef.current = searchTerm
      resultSetter(data);
      loadingSetter(false);
    }, 3000);
    return () => clearTimeout(timeOutId);
  }, [searchTerm]);

  return (
    <Fragment>
      <p>Search Component V1 with ReactGroupTransition + Axios</p>
      <form className="search-wrapper active" onSubmit={e => { e.preventDefault(); searchTermSetter(searchTerm)}}>
        <i className="fas fa-search"/>
        <input className="search-input" type="text" name="search"
               placeholder="Ex: Fibonacci"
               value={searchTerm}
               onChange={e => searchTermSetter(e.target.value)}
        />
      </form>
      <h5 className="my-3">
        {
          !searchTerm && !result?.query?.search?.length ? <div>Please enter a search term!</div> :
          (
            loading ?
              (
                <div>
                  <img alt="loading-svg" height={30} width={30} src={`${process.env.PUBLIC_URL}/loading.svg`}/>
                  <span>Searching for keyword: <b>{searchTerm}</b></span>
                </div>
              ) :
              (
                <div><div>Last searched keyword: <b>{lastSearchKeyWordRef.current}</b></div><div>Total hits: {result?.query?.searchinfo?.totalhits ?? 0}</div></div>
              )
          )
        }
      </h5>
      <ul className="list-group result">
        <TransitionGroup component={null}>
          {
            result?.query?.search.map(r => {
              const itemRef = createRef<any>();
              return (
                <CSSTransition
                  nodeRef={itemRef}
                  key={`card-${r.pageid}`}
                  timeout={800}
                  classNames='list-group-item'>
                  <li className="list-group-item" ref={itemRef} >
                    <div className="content">
                      <div className="title">
                        {r.title}
                      </div>
                      <div className="body">
                        <span dangerouslySetInnerHTML={{__html: r.snippet}}/> ...
                      </div>
                    </div>
                    <a className="btn btn-h-warning text-white"
                       rel="noopener noreferrer"
                       target="_blank"
                       href={`https://en.wikipedia.org?curid=${r.pageid}`}>
                      View Detail
                    </a>
                  </li>
                </CSSTransition>
              )
            })
          }
        </TransitionGroup>
      </ul>
    </Fragment>
  );
};

export default Search;
