import React, { FunctionComponent } from 'react';
import './Link.scss';

const Link: FunctionComponent<{label: string, path: string}> = ({ label, path}) => {
  const onClick = (e:any) => {
    if (e.metaKey || e.ctrlKey) return;

    e.preventDefault();

    if (path === window.location.pathname) return;

    window.history.pushState({}, '', path);
    window.dispatchEvent(new PopStateEvent(NAVIGATION_EVENT))
  }

  return (
    <a className={path === window.location.pathname ? 'active' : ''} onClick={onClick} href={path}>{label}</a>
  );
}

export default Link;
export const NAVIGATION_EVENT = 'NAVIGATION_EVENT';
