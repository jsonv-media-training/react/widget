import React, { FunctionComponent, useEffect, useState } from "react";
import { NAVIGATION_EVENT } from "../link/Link";

const Router: FunctionComponent<{ path: string }> = (props) => {

  const [path, pathSetter] = useState<string>(window.location.pathname);

  useEffect(() => {
    const onNavigation = (e:any) => {
      pathSetter(window.location.pathname)
    };

    window.addEventListener(NAVIGATION_EVENT, onNavigation);
    return () => window.removeEventListener(NAVIGATION_EVENT, onNavigation);
  }, [path]);

  return (
    <div className="mb-4">
      {path === props.path ? props.children : null}
    </div>
  );
}

export default Router;