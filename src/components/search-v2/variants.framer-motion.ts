import { Variants } from "framer-motion";

export const formVariants: Variants = {
  hidden: {
    x: '-100vw'
  },
  show: {
    x: 0,
    transition: {
      type: 'spring',
      ease: 'easeInOut',
      mass: 0.4,
    }
  }
}

export const infoTextVariants: Variants = {
  hidden: {
    rotateX: 90,
    opacity: 0,
  },
  show: {
    rotateX: 0,
    opacity: 1,
    transition: {
      ease: 'backOut',
      delay: 0.5,
    }
  },
  exit: {
    opacity: 0,
    display: 'none'
  }
}

export const loadingTextVariants: Variants = {
  hidden: {
    x: '-100vw',
    opacity: 0,
  },
  show: {
    x: 0,
    opacity: 1,
    transition: {
      ease: 'easeInOut',
    }
  },
  exit: {
    x: '10vw',
    opacity: 0,
    transition: {
      ease: 'backOut',
      duration: 0.5,
    }
  }
}

export const listItemVariants: Variants = {
  open: {
    y: 0,
    opacity: 1,
    transition: {
      y: { stiffness: 1000, velocity: -100 }
    }
  },
  closed: {
    y: 50,
    opacity: 0,
    transition: {
      y: { stiffness: 1000 }
    }
  }
};

export const listWrapperVariants: Variants = {
  open: {
    transition: { staggerChildren: 0.07, delayChildren: 0.2 }
  },
  closed: {
    transition: { staggerChildren: 0.05, staggerDirection: -1 }
  }
};