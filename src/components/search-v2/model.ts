export interface Res<T> {
  data: T;
  status: number;
}

export interface WikiResult {
  error?: any;
  query: {
    searchinfo: { totalhits: number },
    search: QueryResult[]
  }
}

export interface QueryResult {
  ns: number;
  title: string;
  pageid: number;
  size: number;
  wordcount: number;
  snippet: string;
  timestamp: string;
}

export interface SearchState {
  result: null | WikiResult;
  loading: boolean;
  error?: any;
  lastSearchKeyWord?: string
}