import axios from "axios";
import { from, Observable, of } from 'rxjs';
import { Res, SearchState, WikiResult } from "./model";
import { catchError, map } from "rxjs/operators";

export const searchWikiWithRxjs = (searchTerm: string): Observable<SearchState> => {
  return from<Promise<Res<WikiResult>>>(axios.get(
    `https://en.wikipedia.org/w/api.php`,
    {
      params: {
        action: 'query',
        list: 'search',
        format: 'json',
        origin: '*',
        srsearch: searchTerm
      }
    }
  )).pipe(
    map(res => {
      if (res.data.error) throw new Error(res.data.error.info);
      return { result: res.data, loading: false, lastSearchKeyWord: searchTerm }
    }),
    catchError(e => of({ result: null, loading: false, error: e.message }))
  );
};

