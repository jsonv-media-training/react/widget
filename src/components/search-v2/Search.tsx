import React, { FunctionComponent, useEffect, useState, Fragment, useRef } from "react";

import './Search.scss';
import { searchWikiWithRxjs } from "./service";
import { SearchState } from "./model";

import { AnimatePresence, motion } from 'framer-motion';
import { BehaviorSubject, merge, of } from "rxjs";
import { debounceTime, delay, distinctUntilChanged, map, switchMap } from "rxjs/operators";
import { formVariants, infoTextVariants, listItemVariants, listWrapperVariants, loadingTextVariants } from "./variants.framer-motion";

const Search: FunctionComponent<any> = (props) => {
  const [state, stateSetter] = useState<SearchState>();
  const [searchTerm$] = useState<BehaviorSubject<string>>(new BehaviorSubject<any>(''));

  const resultRef = useRef<SearchState>();

  useEffect(() => {
    const observable = searchTerm$.pipe(
      map(s => s.trim()),
      distinctUntilChanged(),
      debounceTime(1500),
      switchMap(s => {
        if (!s && !resultRef.current?.result) {
          return of({ result: null, loading: false })
        }
        if (!s && resultRef.current?.result) {
          return of({ ...resultRef?.current, loading: false})
        }
        return merge(
          of({ result: null, loading: true }),
          searchWikiWithRxjs(s).pipe(delay(1500))
        )
      })
    ).subscribe(state => {
      stateSetter(state);
      resultRef.current = state;
    });

    return () => { observable.unsubscribe(); searchTerm$.unsubscribe(); }
  }, [searchTerm$]);

  return (
    <Fragment>
      <p>Search Component V2 with framer-motion + RXJS</p>
      <motion.form
        variants={formVariants} initial="hidden" animate="show"
        className="search-wrapper active" onSubmit={e => { e.preventDefault(); }}>
        <i className="fas fa-search"/>
        <input className="search-input" type="text" name="search"
               placeholder="Ex: Fibonacci"
               onChange={e => searchTerm$.next(e.target.value)}
        />
      </motion.form>

      <h5 className="my-3" style={{ height: '35px' }}>
        <AnimatePresence exitBeforeEnter>
          {
            !state?.loading && !state?.result &&
            <motion.div key={'info'} variants={infoTextVariants} initial="hidden" animate="show" exit="exit">Please enter a search term!</motion.div>
          }
          {
            state?.loading &&
            <motion.div key={'loading'} variants={loadingTextVariants} initial="hidden" animate="show" exit="exit">
              <img alt="loading-svg" height={30} width={30} src={`${process.env.PUBLIC_URL}/loading.svg`}/>
              <span>Searching for keyword: <b>{searchTerm$.getValue()}</b></span>
            </motion.div>
          }
          {
            !state?.loading && state?.result &&
            <motion.div key={'result'} variants={loadingTextVariants} initial="hidden" animate="show" exit="exit">
              <div>Last searched keyword: <b>{state?.lastSearchKeyWord}</b></div><div>Top 10 results of <b>{state?.result?.query?.searchinfo?.totalhits ?? 0}</b> total hits</div>
            </motion.div>
          }
        </AnimatePresence>
      </h5>
      { state?.result?.query?.search &&
        <motion.ul
          variants={listWrapperVariants} initial="closed" animate="open"
          className="list-group result">
          {
            state?.result?.query?.search.map(r => {
              return (
                <motion.li
                  variants={listItemVariants}
                  whileHover={{ scale: 1.01 }} whileTap={{ scale: 0.95 }}
                  key={r.pageid}
                  className="list-group-item">
                  <div className="content">
                    <div className="title">
                      {r.title}
                    </div>
                    <div className="body">
                      <span dangerouslySetInnerHTML={{__html: r.snippet}}/> ...
                    </div>
                  </div>
                  <a className="btn btn-h-warning text-white"
                     rel="noopener noreferrer"
                     target="_blank"
                     href={`https://en.wikipedia.org?curid=${r.pageid}`}>
                    View Detail
                  </a>
                </motion.li>
              )
            })
          }
        </motion.ul>
      }
    </Fragment>
  );
};

export default Search;
