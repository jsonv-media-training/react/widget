export interface DropDownProps {
  options: DropdownOption<string | number | object>[],
  onOptionSelected?: (e: any, selected: { option: DropdownOption<string | number | object>, index: number }) => void,
  multi?: boolean,
  selectedIndex?: number,
  label?: string
}

export interface DropdownOption<T> {
  label: string,
  value: T,
  // [key: string]: any
}

export enum KEY_CODE {
  ENTER = 13,
  SPACE = 32
}
