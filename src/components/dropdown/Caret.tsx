import React, { FunctionComponent } from 'react';

import { motion } from 'framer-motion';
import { caretVariants } from "./variants.framer-motion";

const Caret: FunctionComponent<{ active: boolean }> = (props) => {
  return (
    <motion.svg
      variants={caretVariants}
      animate={props.active ? 'active': 'inactive'}
      width="25" height="20" viewBox="0 0 80 88" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g id="main-caret">
        <path d="M41.8 63.65C40.7333 64.45 39.2667 64.45 38.2 63.65L7.2 40.4C4.89357 38.6702 6.11696 35 9 35H71C73.883 35 75.1064 38.6702 72.8 40.4L41.8 63.65Z"/>
        <path d="M41.8 63.65C40.7333 64.45 39.2667 64.45 38.2 63.65L7.2 40.4C4.89357 38.6702 6.11696 35 9 35H71C73.883 35 75.1064 38.6702 72.8 40.4L41.8 63.65Z"/>
        <path d="M41.8 63.65C40.7333 64.45 39.2667 64.45 38.2 63.65L7.2 40.4C4.89357 38.6702 6.11696 35 9 35H71C73.883 35 75.1064 38.6702 72.8 40.4L41.8 63.65Z"/>
        <path d="M41.8 63.65C40.7333 64.45 39.2667 64.45 38.2 63.65L7.2 40.4C4.89357 38.6702 6.11696 35 9 35H71C73.883 35 75.1064 38.6702 72.8 40.4L41.8 63.65Z"/>
      </g>
    </motion.svg>
  )
}

export default Caret;