import { Variants } from "framer-motion";

export const caretVariants: Variants = {
  inactive: {
    rotateX: 0,
    transition: {
      duration: 0.5
    }
  },
  active: {
    rotateX: 180,
    transition: {
      duration: 0.5
    }
  }
}

export const optionContainerVariants: Variants = {
  init: {
    height: 0,
    transition: {
      duration: 0.4,
      staggerChildren: 0.02,
      staggerDirection: -1
    }
  },
  in: {
    height: 'auto',
    transition: {
      duration: 0.4,
      staggerChildren: 0.01,
      when: 'beforeChildren'
    }
  },
  out: {
    height: 0,
    transition: {
      duration: 0.3,
      staggerChildren: 0.01,
      staggerDirection: -1,
      // when: 'afterChildren'
    }
  },
  tap: {

  }
}

export const optionVariants: Variants = {
  init: {
    rotateX: -90,
  },
  in: {
    rotateX: 0,
    transition: {
      duration: .6,
      ease: [0.34, 1.56, 0.64, 1]
    }
  },
  out: {
    rotateX: 90,
    opacity: 0,
    transition: {
      duration: 0.6,
      ease: [0.34, 1.56, 0.64, 1]
    }
  },
  tap: {
    backgroundColor: '#000000'
  }
};