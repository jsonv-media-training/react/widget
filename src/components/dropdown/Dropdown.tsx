import React, { FunctionComponent, useRef, useState } from 'react'
import { motion, AnimatePresence } from 'framer-motion';

import Caret from "./Caret";

import { DropDownProps } from "./model";
import './Dropdown.scss';
import { optionVariants, optionContainerVariants } from "./variants.framer-motion";
import { useOnClickOutSide } from "../../utils/custom.hooks";

const Dropdown: FunctionComponent<DropDownProps> = ({ options, label, selectedIndex= -1, onOptionSelected = null  }) => {

  const [active, activeSetter] = useState<boolean>(false);
  const [activeIndex, activeIndexSetter] = useState<number>(selectedIndex);

  const dropdownRef = useRef<HTMLDivElement>(null);
  useOnClickOutSide(dropdownRef, () => activeSetter(false));

  return (
    <div
      ref={dropdownRef}
      onClick={(e) => activeSetter(!active)}
      className="dropdown-container" tabIndex={1}>
      <div className="dropdown-body">
        <div className={`dropdown-title`}>{options[activeIndex]?.label ?? label}</div>
        <Caret active={active}/>
      </div>

      <div className="dropdown-options">
        <AnimatePresence exitBeforeEnter>
          {
            active &&
            <motion.ul
              variants={optionContainerVariants}
              initial='init' animate='in' exit='out'
              key={'options-container'}
              className="options-container">
              {
                options.map((option, index) => {
                  return (
                    <motion.li
                      variants={optionVariants}
                      key={`option-${option.label}`}
                      whileTap='tap'
                      onClick={(e) => { if (onOptionSelected) onOptionSelected(e, { option, index }); activeIndexSetter(index) }}
                      className={`option ${activeIndex === index ? 'active': ''}`}>
                      { option.label }
                    </motion.li>
                  )
                })
              }
            </motion.ul>
          }
        </AnimatePresence>
      </div>
    </div>
  )
}

export default Dropdown;