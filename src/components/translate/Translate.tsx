import React, { FunctionComponent, useEffect, useState } from 'react';
import { motion } from 'framer-motion';
import { BehaviorSubject, of } from "rxjs";
import { debounceTime, delay, distinctUntilChanged, filter, map, switchMap } from "rxjs/operators";

import Dropdown from "../dropdown/Dropdown";
import { translateText } from "./service";
import './Translate.scss';

import { TranslateProps, Translation } from "./model";

const Translate: FunctionComponent<TranslateProps> = ({ options}) => {

  const [targetLanguage, targetLanguageSetter] = useState<string>('');
  const [isLoading, isLoadingSetter] = useState<boolean>(false);
  const [translate$] = useState(new BehaviorSubject<string>(''))
  const [output, outputSetter] = useState<Translation | null>(null);

  useEffect(() => {
    const observable = translate$
      .pipe(
        filter(text => text.length >= 2),
        map(text => text.trim()),
        distinctUntilChanged(),
        debounceTime(2000),
        switchMap(text => {
          if (!!targetLanguage) {
            isLoadingSetter(true);
            return translateText({ text, targetLanguage }).pipe(delay(1000))
          }
          return of([]);
        })
      )
      .subscribe(res => {
        res.length && outputSetter(res[0])
        isLoadingSetter(false);
      });

    return () => observable.unsubscribe()
  }, [translate$, targetLanguage]);
  useEffect(() => { return () => translate$.unsubscribe() }, [translate$]);

  return (
    <div className="translate-container container">
      <div className="row">
        <div className="from col-sm-6">
          <div className="label">
            <b className="font-monospace">From:</b>
            <b>English</b>
          </div>
          <textarea
            onChange={e => translate$.next(e.target.value)}
            rows={6}/>
          <p className="text-muted font-monospace">Must have at least 2 characters</p>
        </div>
        <div className="to col-sm-6">
          <div className="label">
            <b className="font-monospace">To:</b>
            <Dropdown
              label={'Language'}
              options={options}
              onOptionSelected={(e, selected) => targetLanguageSetter(selected.option.value as string)} />
          </div>
          <textarea value={output?.translatedText} readOnly rows={6}/>
          { !targetLanguage && <p className="text-muted font-monospace">Please select a language</p> }
          { isLoading && <p className="text-muted font-monospace">Translating ...</p> }
        </div>
        <div className="img">
          <motion.img
            animate={ isLoading ? { rotateY: 180, transition: { duration: .3, yoyo: Infinity }} : ''  }
            height={50} width={50} alt="img" src={`${process.env.PUBLIC_URL}/translate.svg`}/>
        </div>
      </div>
    </div>
  );
}

export default Translate;