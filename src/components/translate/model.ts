import { DropdownOption } from "../dropdown/model";

export interface TranslateProps {
  options: DropdownOption<string>[]
}

export interface ConvertProps {
  targetLanguage: string,
  text: string
}

export interface Res<T> {
  config?: any;
  data: T;
  status: number;
}

export interface Translation {
  detectedSourceLanguage?: string,
  translatedText?: string
}