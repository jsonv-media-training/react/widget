import axios from 'axios';
import { from, Observable, throwError } from "rxjs";
import { ConvertProps, Res, Translation } from "./model";
import { catchError, map } from "rxjs/operators";
const API_KEY = 'AIzaSyCHUCmpR7cT_yDFHC98CZJy2LTms-IwDlM';

export const translateText = ({ text, targetLanguage }: ConvertProps): Observable<Translation[]> => {
  return from<Promise<Res<{ data: { translations: Translation[] }}>>>(axios.post(
    `https://translation.googleapis.com/language/translate/v2`,
    {},
    {
      params: {
        q: text,
        target: targetLanguage,
        key: API_KEY
      }
    }
  )).pipe(
    map(res => res.data?.data?.translations ?? []),
    catchError(err => throwError(err))
  )
}