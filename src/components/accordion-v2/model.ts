export interface AccordionProps {
  item: AccordionItem,
  toggle: (id: string) => void
}

export enum AccordionToggleMode {
  SINGLE_MODE = 'SINGLE_MODE',
  AUTOCLOSE_MODE = 'AUTOCLOSE_MODE'
}

export interface AccordionAction {
  type: AccordionToggleMode,
  payload: string
}

export interface AccordionWrapperProps {
  items: AccordionItem[],
  toggleMode: AccordionToggleMode
}

export interface AccordionItem {
  id: string,
  title: string,
  text: string,
  isOpen?: boolean
}
