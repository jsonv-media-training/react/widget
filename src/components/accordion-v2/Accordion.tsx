import React, { FunctionComponent, useEffect, useRef } from "react";
import { AccordionProps } from "./model";
import "./Accordion.scss";

const Accordion: FunctionComponent<AccordionProps> = (props) => {

  const accordionBodyRef = useRef<HTMLDivElement>(null);
  const handleAnimation = () => {
    accordionBodyRef!.current!.style.maxHeight = props.item?.isOpen ? `${accordionBodyRef!.current!.scrollHeight}px` : '0px';
  };
  useEffect(handleAnimation, [accordionBodyRef, props.item?.isOpen]);

  return (
    <div className="accordion">
      <div className={`accordion-header pointer ${props.item?.isOpen ? 'up' : 'down'}`} onClick={() => props.toggle(props.item.id)}>
        {props.item.title}
      </div>
      <div ref={accordionBodyRef} className={`accordion-body ${props.item?.isOpen ? 'open': ''}`}>
        {props.item.text}
      </div>
    </div>
  );
};

export default Accordion;