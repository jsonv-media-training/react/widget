import React, { FunctionComponent, useReducer } from "react";
import {AccordionAction, AccordionItem, AccordionToggleMode, AccordionWrapperProps} from "./model";
import Accordion from "./Accordion";
import "./AccordionWrapper.scss"

const reducer = (state: AccordionItem[], action: AccordionAction) => {
  switch (action.type) {
    case AccordionToggleMode.SINGLE_MODE:
      return state.map(item => {
        if (item.id === action.payload) {
          return { ...item, isOpen: !item.isOpen }
        }
        return item
      });
    case AccordionToggleMode.AUTOCLOSE_MODE:
      return state.map(item => {
        if (item.id === action.payload) {
          return { ...item, isOpen: !item.isOpen }
        }
        return { ...item, isOpen: false }
      });
    default:
      return state
  }
}

const AccordionWrapper: FunctionComponent<AccordionWrapperProps> = (props) => {

  const [items, dispatch] = useReducer(reducer, props.items)
  return (
    <div className="accordion-wrapper b-shadow">
      {
        items.map(item =>
          <Accordion key={item.id}
            item={item}
            toggle={(id: string) => dispatch({ type: props.toggleMode, payload: id })}
          />
        )
      }
    </div>
  );
};

export default AccordionWrapper;

