import React, {
  useState,
  useRef,
  useEffect,
  FunctionComponent
} from "react";
import {AccordionProps } from "./model";

import "./Accordion.scss";

const Accordion: FunctionComponent<AccordionProps> = (props) => {

  const [active, activeSetter] = useState(props.item.isOpen ?? false);
  const accordionBodyRef = useRef<HTMLDivElement>(null);
  const handleAnimation = () => {
    accordionBodyRef!.current!.style.maxHeight = active ? `${accordionBodyRef!.current!.scrollHeight}px` : '0px';
  };
  const toggle = () => activeSetter(!active);

  // useEffect(() => console.log(`Accordion render ${JSON.stringify(props.item.title)}`))
  useEffect(handleAnimation, [accordionBodyRef, active]);

  return (
    <div className="accordion">
      <div className={`accordion-header pointer ${active ? 'up' : 'down'}`} onClick={toggle}>
        {props.item.title}
      </div>
      <div ref={accordionBodyRef} className={`accordion-body ${active ? 'open': ''}`}>
        {props.item.text}
      </div>
    </div>
  );
};

export default Accordion;

// const Accordion = forwardRef((props: AccordionProps, ref: Ref<AccordionRef>) => {
//
//   const [active, activeSetter] = useState(props.item.setActive ?? false);
//   const accordionBodyRef = useRef<HTMLDivElement>(null);
//   const handleAnimation = () => {
//     accordionBodyRef!.current!.style.maxHeight = active ? `${accordionBodyRef!.current!.scrollHeight}px` : '0px';
//   };
//   const toggle = () => activeSetter(!active);
//
//   useEffect(() => console.log(`Accordion render ${JSON.stringify(props.item.title)}`))
//   useEffect(handleAnimation, [accordionBodyRef, active]);
//   useImperativeHandle(ref, () => ({ toggle, active, index: props.index }))
//
//   return (
//     <div className="accordion">
//       <div className={`accordion-header pointer ${active ? 'up' : 'down'}`} onClick={toggle}>
//         {props.item.title}
//       </div>
//       <div ref={accordionBodyRef} className={`accordion-body ${active ? 'open': ''}`}>
//         {props.item.text}
//       </div>
//     </div>
//   );
// });
//
// export default Accordion;
