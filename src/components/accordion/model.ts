export interface AccordionProps {
  item: AccordionItem,
  // index: number,
  // onStateChange: Function | null
}

export interface AccordionRef {
  toggle: () => void,
  active: boolean
}

export interface AccordionWrapperProps {
  items: AccordionItem[]
}

export interface AccordionItem {
  id: string,
  title: string,
  text: string,
  isOpen?: boolean
}
