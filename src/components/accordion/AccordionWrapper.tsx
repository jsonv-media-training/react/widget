import React, { FunctionComponent } from "react";
import { AccordionWrapperProps} from "./model";
import Accordion from "./Accordion";
import "./AccordionWrapper.scss"

const AccordionWrapper: FunctionComponent<AccordionWrapperProps> = (props) => {

  // const accordionsRef  = useRef<AccordionRef[]>([])
  // useEffect(() => console.log(props.items))

  return (
    <div className="accordion-wrapper b-shadow">
      {
        props.items.map((item, index) =>
          <Accordion key={item.id}
                    item={item}
                    // index={index}
                    // ref={(element: AccordionRef) => accordionsRef.current[index] = element}
                    // onStateChange={onStateChange}
          />
        )
      }
    </div>
  );
};

export default AccordionWrapper;

