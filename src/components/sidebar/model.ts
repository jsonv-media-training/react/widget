export interface SideBarProps {
  items: Route[]
}

export interface Route {
  label: string;
  path: string;
}