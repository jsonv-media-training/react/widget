import React, { FunctionComponent, useEffect, useRef } from 'react';

import { motion, useCycle } from 'framer-motion';

import './SideBar.scss';
import { SideBarProps } from "./model";
import { navigationContainerVariants, navigationItemVariants, navVariants } from "./variants.framer-motion";
import Link, { NAVIGATION_EVENT } from "../link/Link";
import { useOnClickOutSide } from "../../utils/custom.hooks";

const SideBar: FunctionComponent<SideBarProps> = ({ items }) => {
  const [isOpen, toggleOpen] = useCycle(true, false);

  const navRef = useRef<HTMLElement>(null);
  const dimensions = useRef({ width: navRef?.current?.offsetWidth, height: navRef?.current?.offsetHeight });

  useOnClickOutSide(navRef, () => toggleOpen(1));

  useEffect(() => {
    dimensions.current.width = navRef?.current?.offsetWidth ?? 0;
    dimensions.current.height = navRef?.current?.offsetHeight ?? 0;
  });

  useEffect(() => {
    const onNavigation = () => toggleOpen(1);
    window.addEventListener(NAVIGATION_EVENT, onNavigation);
    return () => window.removeEventListener(NAVIGATION_EVENT, onNavigation);
  }, [toggleOpen])

  return (
    <motion.nav
      ref={navRef}
      variants={navVariants}
      initial={false}
      custom={dimensions.current.height}
      animate={isOpen ? "open" : "closed"}>
      <motion.div/>
      <button className="toggle-btn" onClick={() => toggleOpen()}>
        <svg width="23" height="23" viewBox="0 0 23 23">
        <motion.path
            strokeWidth="3"
            stroke="#ffffff"
            strokeLinecap="round"
            variants={{
              closed: { d: "M 2 2.5 L 20 2.5" },
              open: { d: "M 3 16.5 L 17 2.5" }
            }}
          />
          <motion.path
            strokeWidth="3"
            stroke="#ffffff"
            strokeLinecap="round"
            d="M 2 9.423 L 20 9.423"
            variants={{
              closed: { opacity: 1 },
              open: { opacity: 0 }
            }}
            transition={{ duration: 0.1 }}
          />
          <motion.path
            strokeWidth="3"
            stroke="#ffffff"
            strokeLinecap="round"
            variants={{
              closed: { d: "M 2 16.346 L 20 16.346" },
              open: { d: "M 3 2.5 L 17 16.346" }
            }}
          />
        </svg>
      </button>
      <motion.ul variants={navigationContainerVariants} className="nav-container">
        {items.map(route => (
          // <MenuItem i={i} key={i} />
          <motion.li
            variants={navigationItemVariants}
            whileHover={{ scale: 1.1, originX: 0 }}
            whileTap={{ scale: 0.95 }}
            key={route.label}
            className="nav-item">
              <Link label={route.label} path={route.path}/>
          </motion.li>
        ))}
      </motion.ul>
      {/*<MenuToggle />*/}
    </motion.nav>
  )
}

export default SideBar;